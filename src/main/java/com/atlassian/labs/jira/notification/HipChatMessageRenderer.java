package com.atlassian.labs.jira.notification;

import com.atlassian.labs.jira.dto.NotificationDto;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;
import java.util.Scanner;

public class HipChatMessageRenderer
{
    public static final String NOTIFICATION_TEMPLATE_PATH = "/templates/postfunctions/hip-chat-notification.vm";
    public static final String NOTIFICATION_TEMPLATE_MACROS = readFile("/templates/postfunctions/hip-chat-notification-macros.vm");
    public static final String CONFIG_ERROR_NOTIFICATION_TEMPLATE_PATH = "/templates/postfunctions/hip-chat-notification-error.vm";

    private final TemplateRenderer templateRenderer;

    public HipChatMessageRenderer(final TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    public String renderNotification(final String message, final NotificationDto notificationDto) throws IOException, RenderingException
    {
        return (Strings.isNullOrEmpty(message)) ? renderStandardNotification(notificationDto)
                : renderCustomNotification(message, notificationDto);
    }

    public String renderErrorNotification(final NotificationDto notificationDto) throws IOException
    {
        return renderTemplate(CONFIG_ERROR_NOTIFICATION_TEMPLATE_PATH, ImmutableMap.<String, Object>of("dto", notificationDto));
    }

    private String renderStandardNotification(final NotificationDto notificationDto) throws IOException
    {
        return renderTemplate(NOTIFICATION_TEMPLATE_PATH, ImmutableMap.<String, Object>of("dto", notificationDto));
    }

    private String renderCustomNotification(final String message, final NotificationDto notificationDto) throws RenderingException
    {
        final String templateContents = NOTIFICATION_TEMPLATE_MACROS + " " + parseMessage(message);
        return templateRenderer.renderFragment(templateContents, ImmutableMap.<String, Object>of("dto", notificationDto));
    }

    private String parseMessage(final String message)
    {
        return StringEscapeUtils.escapeHtml(message)
                // Replace carriage returns with <br> tags
                .replace("$issueType", "#issueType()")
                .replace("$issueKey", "#issueKey()")
                .replace("$issueSummary", "#issueSummary()")
                .replace("$issue", "#issue()")
                .replace("$assignee", "#assignee()")
                .replace("$user", "#user()")
                // Replace keywords with macro syntax
                .replace("\n", "<br>")
                .replace("\r", "<br>");
    }

    private String renderTemplate(final String templatePath, final Map<String, Object> context) throws IOException
    {
        final StringWriter messageWriter = new StringWriter();
        templateRenderer.render(templatePath, context, messageWriter);
        return messageWriter.toString();
    }

    private static String readFile(final String filename)
    {
        final StringBuilder fileContents = new StringBuilder();
        final Scanner scanner = new Scanner(HipChatMessageRenderer.class.getResourceAsStream(filename));
        final String lineSeparator = System.getProperty("line.separator");

        try
        {
            while(scanner.hasNextLine())
            {
                fileContents.append(scanner.nextLine()).append(lineSeparator);
            }
            return fileContents.toString();
        }
        finally
        {
            scanner.close();
        }
    }
}
