AJS.toInit(function() {

    var $roomPicker = AJS.$("#hipchatRoomSelect");
    if ($roomPicker.length) {
        new AJS.MultiSelect({
            element: $roomPicker,
            itemAttrDisplayed: "label"
        });
    }

});
